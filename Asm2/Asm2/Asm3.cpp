#include "stdafx.h"
#include <iostream>
using namespace std;
double factorial(int x) {
	double result;
	int currentdegree;
	__asm {
		mov edx, x
		mov currentdegree, edx
		dec currentdegree
		FILD x
		CYCLE:
		mov edx, currentdegree
		cmp edx, 0
		je FINISH
		FILD currentdegree
		FMULP ST(1), ST(0)
		dec currentdegree
		jmp CYCLE
		FINISH :
		FSTP result
	}
	return result;
}
/*
double degree(double x, int y) {
	int currentdegree;
	double basevalue;
	double result;
	__asm {
		FINIT
		FLD x
		FST basevalue
		mov edx, currentdegree
		mov currentdegree, edx
		CYCLE:
		mov edx, currentdegree
		cmp edx, 1
		je FINISH
		FLD basevalue
		FMULP ST(1), ST(0)
		dec currentdegree
		jmp CYCLE
		FINISH:
		FSTP result
	}
	return result;
}*/

double sinx(double x, double epsilon = 1e-5) {
	double MINUSONE = -1;
	double TWO = 2;
	double multiplier = -1;
	double localresult;
	int intBuff;
	int n=1;
	int condition;
	double result=0;

	int currentdegree;
	double basevalue;
	
	__asm {
		FINIT
		CYCLE:
		
		FILD n
		FLD TWO
		FMULP ST(1), ST(0)
		FLD MINUSONE
		FADDP ST(1), ST(0)
		FISTP currentdegree
		
			FLD x
			FSTP basevalue
			jmp DEGREE
			DEGREEFINISH:

		FLD MINUSONE  //
		FLD multiplier
		FMULP ST(1), ST(0)
		FST multiplier
		
		FMULP ST(1), ST(0)   // ���������
			FILD n
			FLD TWO
			FMULP ST(1), ST(0)
			FLD MINUSONE
			FADDP ST(1), ST(0)
			FISTP intBuff
			push intBuff  //
			call dword ptr factorial
			add esp, 4
			FDIVP ST(1), ST(0)
			FST localresult
			FLD result
			FADDP ST(1), ST(0)
			FSTP result
			FLD localresult
			FABS
			FLD epsilon
			FCOMPP
			fstsw ax
			fwait
			sahf
			ja FINISH
			inc n
		jmp CYCLE


		DEGREE :
		FLD basevalue
			CYCLEDEGREE :
		mov edx, currentdegree
			cmp edx, 1
			je DEGREEFINISH
			FLD basevalue
			FMULP ST(1), ST(0)
			dec currentdegree
			jmp CYCLEDEGREE
			
		



		FINISH :
		
	}
	return result;
}
void main1()
{
	double result;
	double x;
	cin >> x;
	result = sinx(x);
	cout << result << endl;
	cin >> x;
}