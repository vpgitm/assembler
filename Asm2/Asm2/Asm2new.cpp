#include "stdafx.h"
#include <iostream>
using namespace std;

float** minorcreation(float** matrix, int msize, int i, int j) {
	int minorsize;
	int memsize;
	int lineNumber;
	int columnNumber;
	int newLineNumber;
	int newColumnNumber;
	float multiplier = 1;
	float MINUSONE = -1;
	float*  pNewLine;
	float** pMinor;
	float*  pLine;
	float buff;
	__asm {
		FINIT
		mov eax, msize             
		mov minorsize, eax
		dec minorsize
		mov eax, 4
		imul eax, minorsize
		mov memsize, eax
		push memsize
		call dword ptr malloc
		add esp, 4
		mov pMinor, eax
		mov newColumnNumber, 0
		MINORCREATION:
			mov edx, newColumnNumber
			cmp minorsize, edx
			je CREATIONCOMPLETE
			push memsize
			call dword ptr malloc
			add esp, 4
			mov esi, newColumnNumber
			mov ebx, pMinor
			mov [ebx][esi * 4], eax
			inc newColumnNumber
			jmp MINORCREATION
		CREATIONCOMPLETE:
				mov newLineNumber, 0
				mov	newColumnNumber, 0
				mov lineNumber, 0
				mov	columnNumber, 0


				MINORFILL:
				mov ebx, minorsize
				cmp newLineNumber, ebx
				je FINISH
				mov ebx, i
				cmp lineNumber, ebx
				je FNEXTLINE
				mov esi, lineNumber
				mov ebx, [matrix]
				mov eax, [ebx][esi * 4]   //
				mov pLine, eax
				mov esi, newLineNumber
				mov edx, pMinor
				mov esi, [edx][esi * 4]
				mov pNewLine, esi
				mov	newColumnNumber, 0
				mov	columnNumber, 0
				MINORFILLLINE :
				mov ebx, minorsize
				cmp newColumnNumber, ebx
				je NEXTLINE
				mov ebx, j
				cmp columnNumber, ebx
				je FNEXTCOLUMN
				mov esi, columnNumber
				mov ebx, pLine
				mov eax, [ebx][esi*4]    //
				mov ebx, -1
				FLD multiplier
				FLD MINUSONE
				FMULP ST(1), ST(0)
				FST multiplier
				mov buff, eax
				FLD buff
				FMULP ST(1), ST(0)
				FSTP buff
				mov eax, buff
				mov esi, newColumnNumber
				mov ebx, pNewLine
				mov [ebx][esi * 4], eax
				inc newColumnNumber
				inc columnNumber
				jmp MINORFILLLINE
				FNEXTLINE:
				inc lineNumber
				jmp MINORFILL
				FNEXTCOLUMN:
				inc columnNumber
				jmp MINORFILLLINE
				NEXTLINE:
				inc lineNumber
				inc newLineNumber
				jmp MINORFILL
				FINISH: 
	}
	return pMinor;
}
void minordeletion(float** pMinor, int msize) {
	int memsize;
	int i;
	__asm {
		mov eax, msize
		mov memsize, eax
		mov ebx, pMinor
		mov i, 0
		CYCLE:
		mov edx, msize
		cmp i, edx
		je FINISH
		mov esi, i
		mov esi, [ebx][esi *4]
		push esi
		call dword ptr free
		add esp, 4
		inc i
		jmp CYCLE
		FINISH:
		mov esi, ebx
		push esi
		call dword ptr free
		add esp, 4
	}
}

float determinant(float** matrix, int msize) {
	float result;
	int elementNumber;
	int minorSize;
	float** pMinor;
	float MINUSONE = -1;
	float multiplier=1;
	float buff;
	__asm {
		FINIT
		cmp msize, 1
		je SIMPLE
		mov elementNumber, 0
		mov result, 0
		CYCLE:
		mov ebx, elementNumber
		cmp msize, ebx
		je FINISH
		push elementNumber
		push 0
		push msize
		push matrix
		call dword ptr minorcreation
		add esp, 16
		mov pMinor, eax
		mov ebx, msize
		mov minorSize, ebx
		dec minorSize
		push minorSize
		push pMinor
		call dword ptr determinant
		add esp, 8
		
		FLD MINUSONE
		FLD multiplier

		FST multiplier
		//mov ebx, [eax]
		//mov buff, ebx
		//FLD buff         � ��� �����
		FMULP ST(1), ST(0)         //��������� ���� 
		FST multiplier
		mov esi, elementNumber
		mov edx, [matrix]
		mov ebx, [edx]
		mov edx, [ebx][esi * 4]
		mov buff, edx
		mov edx, ebx
		FLD buff
		FMULP ST(1), ST(0)           //"�" �� ������
		FMULP ST(1), ST(0)           // ������������
		FLD result
		FADDP  ST(1), ST(0)
		FSTP result
		inc elementNumber
		push minorSize
		push pMinor
		call dword ptr minordeletion
		add esp, 8
		jmp CYCLE

		SIMPLE:
		mov esi, [matrix]
		mov ebx, [esi]
		mov esi, [ebx]
		mov result, esi
		jmp FINISH
		FINISH:

	}
	return result;
}
float determinantColumn(float** matrix, int msize, int column) {
	float result;
	
	float** pMinor;
	int line;
	float MINUSONE = -1;
	float multiplier=1;
	float a;
	int minorsize;
	__asm {
		FINIT
		mov edx, msize
		mov minorsize, edx
		dec minorsize
		mov result, 0
		mov line, 0
		DECOMPOZITION:
		mov edx, line
			cmp msize, edx
			je FINISH
			mov ebx, [matrix]
			mov ecx, line
			mov esi, [ebx][ecx * 4]
			mov edx, column
			mov ebx, [esi][edx * 4]
			mov a, ebx
			push column
			push ecx
			push msize
			push matrix
			call dword ptr minorcreation
			mov pMinor, eax
			add esp, 16
			push minorsize
			push pMinor
			call dword ptr determinant
			add esp, 8
			FLD MINUSONE
			FLD multiplier
			FST multiplier
			FMULP ST(1), ST(0)         //��������� ���� 
			FST multiplier
			FLD a
			FMULP ST(1), ST(0)           //"�" �� ������
			FMULP ST(1), ST(0)           // ������������
			FLD result
			FADDP  ST(1), ST(0)
			FSTP result
			push minorsize
			push pMinor
			call dword ptr minordeletion
			add esp, 8
			inc line
			jmp DECOMPOZITION

			FINISH :
	}
	return result;
}
void main() {
	float result;
	int SIZE;
	int column;
	float x;
	cout << "Set Size" << endl;
	cin >> SIZE;
	cout << "Set Column" << endl;
	cin >> column;
	float** matrix = new float*[SIZE];
	for (int i = 0; i < SIZE; i++)
	{
		matrix[i] = new float[SIZE];
	}
	for (int i = 0; i < SIZE; i++) 
	{
		for (int j = 0; j < SIZE; j++) 
		{
			cout << "Set element "<< i <<" "<< j << endl;
			cin >> x;
			matrix[i][j] = x;
		}
	}
	
	result=determinantColumn(matrix, SIZE, column);
	cout << result << endl;
	cin >> x;

}